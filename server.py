#!/usr/bin/python
from socket import *
import os

serverPort = 12002
serverSocket = socket(AF_INET,SOCK_STREAM)
serverSocket.bind(('',serverPort))
serverSocket.listen(1)
print('The server is ready to receive')
while True:
    connectionSocket, addr = serverSocket.accept()
    print('new request received from')
    print(addr)
    print('connectionSocket is'); print(connectionSocket);

    while(1):
        commands = connectionSocket.recv(1024).decode()
        commands_lists = commands.split()
        if commands_lists[0] == 'ls':
            new_path = os.getcwd()
            dirpath = os.listdir(new_path)
            path = str(dirpath)
            connectionSocket.send(path.encode())
            print("Done!!!")

        elif  commands_lists[0] == 'cd':
            os.chdir(commands_lists[1])
            dirpath1 = os.listdir()
            path1 = str(dirpath1)
            connectionSocket.send(path1.encode())
            print("Done!!!")


        elif commands_lists[0] == 'get':
            filename = commands_lists[1]
            f = open(filename, 'rb')
            l = f.read(1024)
            while (l):
                connectionSocket.send(l)
                l = f.read(1024)
            f.close()

            print('\nDone sending')
            #connectionSocket.close()
            break


        elif commands_lists[0] == 'put':
            with open(commands_lists[1], 'wb') as f:
                while True:
                    data = connectionSocket.recv(1024)
                    if not data:
                        break
                    f.write(data)

            print('\nFile Received Sucessfully!!!')

        elif commands_lists[0] == 'delete':
            os.remove(commands_lists[1])
            s = "Deleted!!!"
            connectionSocket.send(str(s).encode())
            print("Deleted!!!")

        elif commands_lists[0] == 'rename':
            os.rename(commands_lists[1],commands_lists[2])
            s = "Renamed!!!"
            connectionSocket.send(str(s).encode())
            print("Renamed!!!")

        elif commands_lists[0] == 'exit' or commands_lists[0] == 'quit' or commands_lists[0] == 'bye':
            connectionSocket.close()
            print("\nConnection Closed!!!")
            break

        else:
            break

print("Bye!!!")

