#ftp1.at.proftpd.org

import ftplib


def ftp_connect():
            host1 = input('Please enter FTP address: ')
            username1 = input('Please enter Username: ')
            password1 = input('Please enter Password: ')
            with ftplib.FTP(host1) as ftp:
                try:
                    ftp.login(username1,password1)
                    print(ftp.getwelcome())
                    print('Current Directory', ftp.pwd())
                    ftp.dir()
                    print('\n************************'
                          '\nCommands: '
                          '\ncd (eg: cd abc)'
                          '\nget (eg: get abc.txt)'
                          '\nput (eg: put abc.txt)'
                          '\nls (eg: ls)'
                          '\nrename (eg: rename abc.txt def.txt)'
                          '\ndelete (eg: delete)'
                          '\nexit/bye/quit (eg: exit)'
                          '\nhelp (eg: help)'
                          '\n************************')
                    ftp_command(ftp)
                except ftplib.all_errors as e:
                    print(e)


def ftp_command(ftp):
    while True:
        command = input('Enter a command: ')
        commands_list = command.split()


        #Change directory
        if commands_list[0] == 'cd':
                try:
                    ftp.cwd(commands_list[1])
                    print('Directory of', ftp.pwd())
                    ftp.dir()
                    print('Current Directory', ftp.pwd())
                except ftplib.all_errors as e:
                    print(e)


        #Downloading File
        elif commands_list[0] == 'get':
                try:
                    ftp.retrbinary('RETR ' + commands_list[1], open(commands_list[1], 'wb').write)
                    print('File Successfully downloaded!!!')
                except ftplib.all_errors as e:
                    print(e)


        #Uploading File
        elif commands_list[0] == 'put':
                try:
                    ftp.storlines('STOR ' + commands_list[1], open(commands_list[1], 'rb').read)
                    print('File Successfully uploaded!!!')
                except ftplib.all_errors as e:
                    print(e)


        #Listing Directory
        elif commands_list[0] == 'ls':
            try:
                print('Directory of', ftp.pwd())
                ftp.dir()
            except ftplib.all_errors as e:
                print(e)


        #Exit
        elif commands_list[0] == 'exit' or commands_list[0] == 'bye' or commands_list[0] == 'quit':
            try:
                print("GoodBye!!!")
                ftp.quit()
                break
            except ftplib.all_errors as e:
                print(e)


        #Rename
        elif commands_list[0] == 'rename':
            try:
                fromname = commands_list[1]
                toname = commands_list[2]
                ftp.rename(fromname, toname)
                print("Renamed Sucessfully!!!")
            except ftplib.all_errors as e:
                print(e)


        #Delete
        elif commands_list[0] == 'delete':
            try:
                ftp.delete(commands_list[1])
            except ftplib.all_errors as e:
                print(e)


        #Help
        elif commands_list[0] == 'help':
            print('\n************************'
                  '\nCommands: '
                  '\ncd (eg: cd abc)'
                  '\nget (eg: get abc.txt)'
                  '\nput (eg: put abc.txt)'
                  '\nls (eg: ls)'
                  '\nrename (eg: rename abc.txt def.txt)'
                  '\ndelete (eg: delete)'
                  '\nexit/bye/quit (eg: exit)'
                  '\nhelp (eg: help)'
                  '\n************************')


        #Invalid
        else:
            print('Invalid command!!!')


ftp_connect()