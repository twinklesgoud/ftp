#!/usr/bin/python
from socket import *
serverName = '127.0.0.1'
serverPort = 12002
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((serverName,serverPort))

print('\n************************'
      '\nCommands: '
      '\ncd (eg: cd abc)'
      '\nget (eg: get abc.txt)'
      '\nput (eg: put abc.txt)'
      '\nls (eg: ls)'
      '\nrename (eg: rename abc.txt def.txt)'
      '\ndelete (eg: delete)'
      '\nexit/bye/quit (eg: exit)'
      '\nhelp (eg: help)'
      '\n************************')

while(1):
    commands = input("\nEnter the Command: ")
    commands_lists = commands.split()
    clientSocket.send(commands.encode())

    if commands_lists[0] == 'ls':
        files_lists = clientSocket.recv(1024)
        print('List of Files: \n', files_lists.decode())
        print("Done!!!")

    elif commands_lists[0] == 'cd':
        dirpath_receive = clientSocket.recv(1024)
        print('List of Files: \n', dirpath_receive.decode())
        print('Done!!!')



    elif commands_lists[0] == 'get':
        with open(commands_lists[1], 'wb') as f:
            while True:
                data = clientSocket.recv(1024)
                if not data:
                    break
                f.write(data)

        print('\nFile Received Sucessfully!!!')



    elif commands_lists[0] == 'put':
        filename = commands_lists[1]
        f = open(filename, 'rb')
        l = f.read(1024)
        while (l):
            clientSocket.send(l)
            l = f.read(1024)
        f.close()

        print('\nFile Sent Sucessfully!!!')

    elif commands_lists[0] == 'help':
        print('\n************************'
              '\nCommands: '
              '\ncd (eg: cd abc)'
              '\nget (eg: get abc.txt)'
              '\nput (eg: put abc.txt)'
              '\nls (eg: ls)'
              '\nrename (eg: rename abc.txt def.txt)'
              '\ndelete (eg: delete)'
              '\nexit/bye/quit (eg: exit)'
              '\nhelp (eg: help)'
              '\n************************')

    elif commands_lists[0] == 'exit' or commands_lists[0] == 'bye' or commands_lists[0] == 'quit':
        clientSocket.close()
        print("Connection Closed!!!")
        break

    elif commands_lists[0] == 'delete':
        ack = clientSocket.recv(1024)
        print(ack)

    elif commands_lists[0] == 'rename':
        ack = clientSocket.recv(1024)
        print(ack)
        break

    else:
        print('Invalid Command!!!')
        break

print("Bye!!!")







